package com.cms.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.cms.entity.Commission;
import com.cms.entity.Member;
import com.cms.entity.Order;
import com.cms.entity.OrderItem;

public class DistributeUtils {

	public static void commission(Order order){
        List<Commission> commissions = new Commission().dao().findByOrderId(order.getId());
        if(CollectionUtils.isNotEmpty(commissions)){
            return;
        }
        BigDecimal amount = order.getAmount();
        Member member = order.getMember();
        Long[] parentIds= member.getParentIds();
        Integer grade = parentIds.length;
        String title = "";
        String image = "";
        List<OrderItem> orderItems = order.getOrderItems();
        for(OrderItem orderItem : orderItems){
            title =  orderItem.getName();
            image = orderItem.getImage();
            break;
        }
        //一级分销
        if(grade>0){
            //一级金额
        	Member oneGradeMember = member.getParent();
        	BigDecimal oneGradeLevel = oneGradeMember.getDistributeRank().getOneGrade();
            BigDecimal oneGradeLevelAmount = amount.multiply(oneGradeLevel.divide(new BigDecimal(100)));
            Commission commission = new Commission();
            commission.setTitle(title);
            commission.setImage(image);
            commission.setCreateDate(new Date());
            commission.setModifyDate(new Date());
            commission.setAmount(amount);
            commission.setOrderId(order.getId());
            commission.setMemberId(oneGradeMember.getId());
            commission.setLevel(oneGradeLevel+"%");
            commission.setGrade(1);
            commission.setDistributeAmount(oneGradeLevelAmount);
            commission.setStatus(0);
            commission.save();
        }
        //二级分销
        if(grade>1){
            //二级金额
        	Member twoGradeMember = member.getParent().getParent();
        	BigDecimal twoGradeLevel = twoGradeMember.getDistributeRank().getTwoGrade();
            BigDecimal twoGradeLevelAmount = amount.multiply(twoGradeLevel.divide(new BigDecimal(100)));
            Commission commission = new Commission();
            commission.setTitle(title);
            commission.setImage(image);
            commission.setCreateDate(new Date());
            commission.setModifyDate(new Date());
            commission.setAmount(amount);
            commission.setOrderId(order.getId());
            commission.setMemberId(twoGradeMember.getId());
            commission.setLevel(twoGradeLevel+"%");
            commission.setGrade(2);
            commission.setDistributeAmount(twoGradeLevelAmount);
            commission.setStatus(0);
            commission.save();
        }
        //三级分销
        if(grade>2){
            //三级金额
        	Member threeGradeMember = member.getParent().getParent().getParent();
        	BigDecimal threeGradeLevel = threeGradeMember.getDistributeRank().getThreeGrade();
            BigDecimal threeGradeLevelAmount = amount.multiply(threeGradeLevel.divide(new BigDecimal(100)));
            Commission commission = new Commission();
            commission.setTitle(title);
            commission.setImage(image);
            commission.setCreateDate(new Date());
            commission.setModifyDate(new Date());
            commission.setAmount(amount);
            commission.setOrderId(order.getId());
            commission.setMemberId(threeGradeMember.getId());
            commission.setLevel(threeGradeLevel+"%");
            commission.setGrade(3);
            commission.setDistributeAmount(threeGradeLevelAmount);
            commission.setStatus(0);
            commission.save();
            
        }
	}
}
