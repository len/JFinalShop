//package com.cms.util;
//
//import java.io.File;
//import java.io.IOException;
//import java.net.URL;
//import java.util.Date;
//import java.util.UUID;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import javax.imageio.ImageIO;
//
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang.StringUtils;
//import org.apache.commons.lang.time.DateFormatUtils;
//import org.apache.commons.lang.time.DateUtils;
//
//import com.cms.CommonAttribute;
//import com.cms.Distribute;
//import com.cms.PosterImage;
//import com.cms.entity.Member;
//import com.jfinal.kit.PathKit;
//
//public class PosterImageUtils {
//    
//    //正则
//    public static final String POSTER_REGEX="<div class=\"item\" style=\"width: ([\\s\\S]*?)px; height: ([\\s\\S]*?)px; z-index: ([\\s\\S]*?); left: ([\\s\\S]*?)px; top: ([\\s\\S]*?)px;\"><div class=\"form-control\"><img style=\"width:([\\s\\S]*?)px;height:([\\s\\S]*?)px;\" src=\"([\\s\\S]*?)\">";
//    
//    /** 头像 */
//    public static final String POSTER_IMAGE_TYPE_AVATAR="avatar";
//    
//    /** 二维码 */
//    public static final String POSTER_IMAGE_TYPE_QR="qr";
//    
//    
//    public static PosterImage getPosterImage(String type,String posterContent){
//        Pattern pat = Pattern.compile(POSTER_REGEX);
//        Matcher mat = pat.matcher(posterContent);
//        while (mat.find()) {
//            Integer width = Integer.valueOf(mat.group(1).trim());
//            Integer height = Integer.valueOf(mat.group(2).trim());
//            Integer index = Integer.valueOf(mat.group(3).trim());
//            Integer left = Integer.valueOf(mat.group(4).trim());
//            Integer top = Integer.valueOf(mat.group(5).trim());
//            String src = mat.group(8).trim();
//            
//            PosterImage posterImage = new PosterImage();
//            posterImage.setWidth(width);
//            posterImage.setHeight(height);
//            posterImage.setIndex(index);
//            posterImage.setLeft(left);
//            posterImage.setTop(top);
//            posterImage.setSrc(src);
//            if(POSTER_IMAGE_TYPE_AVATAR.equals(type) && "/static/img/qr.jpg".equals(src)){
//                return posterImage;
//            }else if("qr".equals(type) && "/static/img/avatar.jpg".equals(src)){
//                return posterImage;
//            }
//            
//        }
//        return null;
//    }
//    
//    public static PosterImage getAvatar(String posterContent){
//        return getPosterImage(POSTER_IMAGE_TYPE_AVATAR,posterContent);
//    }
//    
//    public static PosterImage getQr(String posterContent){
//        return getPosterImage(POSTER_IMAGE_TYPE_QR,posterContent);
//    }
//    
//    public static void build(Long memberId) {
//        Member member = new Member().dao().findById(memberId);
//        String weixinOpenId = member.getWeixinOpenId();
//        Distribute distribute = SystemUtils.getDistribute();
//        String posterContent = distribute.getContent();
//        String background = distribute.getBackground();
//        String posterExpireDate="";
//        if(member.getWeixinQrDate()!=null){
//            if((member.getWeixinQrDate()+2592000)>new Date().getTime()/1000){
//                posterExpireDate=DateFormatUtils.format((member.getWeixinQrDate()+2592000)*1000, "MM月dd日 HH:mm:ss");
//            }else{
//                posterExpireDate=DateFormatUtils.format(DateUtils.addDays(new Date(), 30), "MM月dd日 HH:mm:ss");
//            }
//        }else{
//            posterExpireDate=DateFormatUtils.format(DateUtils.addDays(new Date(), 30), "MM月dd日 HH:mm:ss");
//        }
//        String textMessage = WeixinUtils.getTextMessage(member.getWeixinOpenId(), "你的海报将于"+posterExpireDate+"失效，过期后请点击 [我的海报] 菜单重新获取哦。\n\n正在为您发送海报，请稍后...");
//        WeixinUtils.customSend(textMessage);
//        
//        if(StringUtils.isNotBlank(member.getWeixinQrMediaId())){
//            if(member.getWeixinQrDate()>DateUtils.addDays(new Date(), -3).getTime()/1000){
//                String imageMessage = WeixinUtils.getImageMessage(member.getWeixinOpenId(), member.getWeixinQrMediaId());
//                WeixinUtils.customSend(imageMessage);
//                return;
//            }
//        }
//        
//        File waterFile=null;
//        if(StringUtils.isNotBlank(member.getWeixinQr()) && (member.getWeixinQrDate()+2592000)>new Date().getTime()/1000){
//            waterFile = new File(PathKit.getWebRootPath()+member.getWeixinQr());
//        }else{
//            try{
//                String tempPath = FileUtils.getTempDirectoryPath();
//                File qrcodeTempFile = new File(tempPath+ "/" + UUID.randomUUID().toString() + "." + "jpg") ;
//                String imageUploadPath = "/"+CommonAttribute.UPLOAD_PATH;
//                String qrcodePath = imageUploadPath+"/"+"qrcode_"+memberId+".jpg";
//                String qrcodeContent = WeixinUtils.getQrcodeContent(memberId);
//                String ticket = WeixinUtils.getQrcodeCreate(qrcodeContent);
//                String qrcodeUrl = WeixinUtils.getShowqrcodeUrl(ticket);
//                WebUtils.get(qrcodeUrl, qrcodeTempFile, null);
//                
//                waterFile = new File(PathKit.getWebRootPath()+qrcodePath);
//                FileUtils.copyFile(qrcodeTempFile, waterFile);
//                FileUtils.deleteQuietly(qrcodeTempFile);
//                member.setWeixinQr(qrcodePath);
//                member.setWeixinQrDate(new Date().getTime()/1000);
//                
//            }catch (IOException e) {
//                throw new RuntimeException(e.getMessage(), e);
//            } 
//        }
//        System.out.println("posterContent=="+posterContent);
//        PosterImage avatarPosterImage = getAvatar(posterContent);
//        PosterImage qrPosterImage = getQr(posterContent);
//        
//        try{
//            String tempPath = FileUtils.getTempDirectoryPath();
//            File avatarTempFile = new File(tempPath + "/" + UUID.randomUUID().toString() + "." + "jpg");
//            File backgroundFile = new File(PathKit.getWebRootPath()+background);
//            ImageUtils.addWatermark(backgroundFile, avatarTempFile, ImageIO.read(new URL(member.getAvatar())),avatarPosterImage.getLeft(),avatarPosterImage.getTop(),
//                    avatarPosterImage.getWidth(),avatarPosterImage.getHeight());
//            File qrTempFile = new File(tempPath + "/" + UUID.randomUUID().toString() + "." + "jpg");
//            ImageUtils.addWatermark(avatarTempFile, qrTempFile, ImageIO.read(waterFile),qrPosterImage.getLeft(),qrPosterImage.getTop(),
//                    qrPosterImage.getWidth(),qrPosterImage.getHeight());
//            File posterFile = new File(tempPath + "/upload_" + weixinOpenId + "." + "jpg");
//            FileUtils.copyFile(qrTempFile, posterFile);
//            String mediaId =  WeixinUtils.uploadImage(posterFile);
//            String imageMessage = WeixinUtils.getImageMessage(weixinOpenId, mediaId);
//            WeixinUtils.customSend(imageMessage);
//            
//            //member
//            member.setWeixinQrMediaId(mediaId);
//            member.update();
//            
//            FileUtils.deleteQuietly(avatarTempFile);
//            FileUtils.deleteQuietly(qrTempFile);
//            FileUtils.deleteQuietly(posterFile);
//            
//        }catch (Exception e) {
//            // TODO: handle exception
//            e.printStackTrace();
//        }
//    }
//}
