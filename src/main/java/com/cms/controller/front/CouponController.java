package com.cms.controller.front;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.cms.Feedback;
import com.cms.entity.Coupon;
import com.cms.entity.Member;
import com.cms.entity.MemberCoupon;
import com.cms.entity.Order;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/coupon")
public class CouponController extends BaseController{
	
	/**
	 * 首页
	 */
	public void list(){
		List<Coupon> coupons = new Coupon().dao().findList(new Date());
		setAttr("coupons", coupons);
	    render("/templates/"+getTheme()+"/"+getDevice()+"/coupon_list.html");
	}
	
	public void choose(){
		setAttr("cart", getCurrentCart());
		List<Coupon> coupons = new Coupon().dao().findByMemberId(getCurrentMember().getId());
		setAttr("coupons", coupons);
	    render("/templates/"+getTheme()+"/"+getDevice()+"/coupon_choose.html");
	}
	
	public void receive(){
		Member currentMember = getCurrentMember();
		if(currentMember==null){
			renderJson(Feedback.warn("请登录"));
			return;
		}
		Long couponId = getParaToLong("couponId");
		MemberCoupon memberCoupon = new MemberCoupon().dao().findByMemberId(currentMember.getId(), couponId);
		if(memberCoupon!=null){
			renderJson(Feedback.error("您已经领取过了"));
			return;
		}
		memberCoupon = new MemberCoupon();
		memberCoupon.setCreateDate(new Date());
		memberCoupon.setMemberId(currentMember.getId());
		memberCoupon.setCouponId(couponId);
		memberCoupon.save();
		renderJson(Feedback.success(new HashMap<>()));
	}


}
