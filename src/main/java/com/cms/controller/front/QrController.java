package com.cms.controller.front;

import java.util.HashMap;
import java.util.Map;

import com.cms.Feedback;
import com.cms.entity.Member;
import com.cms.entity.Product;
import com.cms.routes.RouteMapping;
import com.cms.util.ProductUtils;
import com.cms.util.SystemUtils;

@RouteMapping(url = "/qr")
public class QrController extends BaseController{

	public void create(){
		Member currentMember = getCurrentMember();
		if(currentMember==null){
			renderJson(Feedback.error("请登录"));
			return;
		}
		String qrcode = currentMember.getCode();
		String avatar = currentMember.getAvatar();
		String nickname = currentMember.getNickname();
		Long productId = getParaToLong("productId");
		Product product = new Product().dao().findById(productId);
		String productImg = SystemUtils.getConfig().getImgUrl()+product.getImage();
		String productName = product.getName();
		String productPrice = product.getPrice().toString();
		Integer type = getParaToInt("type");
		Map<String,Object> result = new HashMap<>();
		if(type==1){
			String url = SystemUtils.getConfig().getSiteUrl()+"/product/detail/"+productId+"?qrcode="+qrcode;
			String image = ProductUtils.qrImage(avatar, nickname, productImg, productName, productPrice, url);
			result.put("qrImage", SystemUtils.getConfig().getImgUrl()+image);
		}else if(type==2){
			String url = SystemUtils.getConfig().getSiteUrl()+"/pintuan/detail/"+productId+"?qrcode="+qrcode;
			String image = ProductUtils.qrImage(avatar, nickname, productImg, productName, productPrice, url);
			result.put("qrImage", SystemUtils.getConfig().getImgUrl()+image);
		}
		renderJson(Feedback.success(result));
	}
}
