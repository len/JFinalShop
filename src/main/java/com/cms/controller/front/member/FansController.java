package com.cms.controller.front.member;

import com.cms.controller.front.BaseController;
import com.cms.entity.Member;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/member/fans")
public class FansController extends BaseController{
	
	
	/**
	 * 
	 */
	public void index(){
		setAttr("todayFansCount", new Member().dao().findTodayFansCount(getCurrentMember().getId()));
		setAttr("monthFansCount", new Member().dao().findMonthFansCount(getCurrentMember().getId()));
		setAttr("fansCount", new Member().dao().findFansCount(getCurrentMember().getId()));
		setAttr("fansMembers", new Member().dao().findFans(getCurrentMember().getId()));
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_fans.html");
	}
	
	
	/**
	 * 列表
	 */
	public void list(){
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber=1;
		}
		int pageSize = 10 ; 
		Member currentMember = getCurrentMember();
		setAttr("page",new Member().dao().findFansPage(pageNumber,pageSize,currentMember.getId()));
		setAttr("todayFansCount", new Member().dao().findTodayFansCount(getCurrentMember().getId()));
		setAttr("monthFansCount", new Member().dao().findMonthFansCount(getCurrentMember().getId()));
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_fans_list.html");
	}
}
