package com.cms.controller.xcx;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cms.Feedback;
import com.cms.controller.front.BaseController;
import com.cms.entity.Cart;
import com.cms.entity.CartItem;
import com.cms.routes.RouteMapping;

/**
 * @description : 
 * @author : heyewei
 * @create : 2020年9月22日
 **/
@RouteMapping(url = "/xcx/cart")
public class CartController extends BaseController{

	public void index(){
		Cart currentCart = getCurrentCart();
		int totalQuantity = currentCart.getTotalQuantity();
		BigDecimal totalPrice = currentCart.getTotalPrice();
		List<CartItem> selectCartItems = currentCart.getSelectCartItems();
		Map<String,Object> map = new HashMap<>();
		map.put("selectCartItems", selectCartItems);
		map.put("totalQuantity",totalQuantity);
		map.put("totalPrice",totalPrice);
		renderJson(Feedback.success(map));
	}
}
