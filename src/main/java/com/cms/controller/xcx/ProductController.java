package com.cms.controller.xcx;

import java.util.HashMap;
import java.util.Map;

import com.cms.Feedback;
import com.cms.controller.front.BaseController;
import com.cms.entity.Product;

/**
 * @description : 
 * @author : heyewei
 * @create : 2020年12月22日
 **/
public class ProductController extends BaseController{

	public void index(){
		Long productId = getParaToLong("productId");
		Product product = new Product().dao().findById(productId);
		Map<String,Object> map = new HashMap<>();
		map.put("product", product);
		renderJson(Feedback.success(map));
	}
}
