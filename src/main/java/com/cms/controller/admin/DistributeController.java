package com.cms.controller.admin;

import com.cms.entity.Distribute;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/admin/distribute")
public class DistributeController extends BaseController {

    /**
     * 编辑
     */
    public void edit(){
    	Long id = getParaToLong("id");
        setAttr("distribute", new Distribute().dao().findById(id));
        render(getView("distribute/edit"));
    }
    
    /**
     * 更新
     */
    public void update(){
        Distribute distribute = getModel(Distribute.class, "",true);
        distribute.update();
        redirect("/admin/distribute/edit?id="+distribute.getId());
    }
}
