/*
 * 
 * 
 * 
 */
package com.cms.controller.admin;

import com.cms.Feedback;
import com.cms.entity.Complaint;
import com.cms.routes.RouteMapping;
import org.apache.commons.lang.ArrayUtils;

import java.util.Date;
import java.util.HashMap;


/**
 * Controller - 投诉
 * 
 * 
 * 
 */
@RouteMapping(url = "/admin/complaint")

public class ComplaintController extends BaseController {



	/**
	 * 列表
	 */
	public void list() {
	    String name = getPara("name");
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber = 1;
		}
		setAttr("page", new Complaint().dao().findPage(name,pageNumber,PAGE_SIZE));
		setAttr("name", name);
		render(getView("complaint/list"));
	}

	/**
	 * 删除
	 */
	public void delete() {
		Long ids[] = getParaValuesToLong("ids");
		if(ArrayUtils.isNotEmpty(ids)){
			for(Long id:ids){
				new Complaint().dao().deleteById(id);
			}
		}
		renderJson(Feedback.success(new HashMap<>()));
	}

}