package com.cms.controller.admin;

import com.cms.entity.Freight;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/admin/freight")
public class FreightController extends BaseController {
	
    /**
     * 编辑
     */
    public void edit(){
    	Long id = getParaToLong("id");
        setAttr("freight", new Freight().dao().findById(id));
        render(getView("freight/edit"));
    }
    
    /**
     * 更新
     */
    public void update(){
    	Freight freight = getModel(Freight.class, "",true);
    	freight.update();
        redirect("/admin/freight/edit?id="+freight.getId());
    }

}
