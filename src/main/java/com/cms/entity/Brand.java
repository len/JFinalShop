package com.cms.entity;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.cms.entity.base.BaseBrand;
import com.cms.util.DBUtils;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Page;

/**
 * Entity - 品牌
 * 
 * 
 * 
 */
@SuppressWarnings("serial")
public class Brand extends BaseBrand<Brand> {
	
    /**
     * 商品
     */
    private List<Product> hotProducts;
    
    /**
     * 获取商品
     * 
     * @return 商品
     */
    public List<Product> getHotProducts(){
        if(hotProducts == null){
        	hotProducts = new Product().dao().find("select * from cms_product where brandId=? limit 3",getId());
        }
        return hotProducts;
    }
    
    /**
     * 商品
     */
    private List<Product> products;
    
    /**
     * 获取商品
     * 
     * @return 商品
     */
    public List<Product> getProducts(){
        if(products == null){
        	products = new Product().dao().find("select * from cms_product where brandId=? and isMarketable=1",getId());
        }
        return products;
    }
    
    
	public Page<Brand> findPage(String name,Integer pageNumber,Integer pageSize){
	    String filterSql = "";
	    if(StringUtils.isNotBlank(name)){
            filterSql+= " and name like '%"+name+"%'";
        }
		String orderBySql = DBUtils.getOrderBySql("createDate desc");
		return paginate(pageNumber, pageSize, "select *", "from cms_brand where 1=1 "+filterSql+orderBySql);
	}
    
    
	/**
	 * 查找友情链接
	 * 
	 * @param orderBy
	 *            排序
	 * @param start
     *            起始位置
     * @param count
     *            数量
	 * @return 友情链接
	 */
	public List<Brand> findList(String orderBy,Integer start,Integer count) {
		String orderBySql = "";
		if(StringUtils.isBlank(orderBy)){
		    orderBySql = DBUtils.getOrderBySql("sort desc,createDate desc");
		}else{
		    orderBySql = DBUtils.getOrderBySql(orderBy);
		}
		String countSql=DBUtils.getCountSql(start, count);
		return find("select * from cms_brand where 1=1 "+orderBySql+countSql);
	}
	
	
    /**
     * 获取路径
     * 
     * @return 路径
     */
    public String getPath() {
        return JFinal.me().getContextPath()+"/brand/detail/"+getId();
    }
}
