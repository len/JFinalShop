package com.cms.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.annotation.JSONField;
import com.cms.entity.base.BasePack;

/**
 * 包裹
 */
@SuppressWarnings("serial")
public class Pack extends BasePack<Pack> {
	/**
	 *	快递公司编码
	 */
	public enum ExpressCode{
		SFEXPRESS("顺丰"),
		YTO("圆通"),
		ZTO("中通"),
		STO("申通"),
		HTKY("汇通快递"),
		YUNDA("韵达快递"),
		CHINAPOST("邮政包裹");
		public String text;
		ExpressCode(String text){
			this.text = text;
		}
		public String getText(){
			return this.text;
		} 
	    public static Map<Integer, Pack.ExpressCode> expressCodeValueMap = new HashMap<>();
	    static {
			Pack.ExpressCode[] values = Pack.ExpressCode.values();
	        for (Pack.ExpressCode expressCode : values) {
	        	expressCodeValueMap.put(expressCode.ordinal(), expressCode);
	        }
	    }
	}
	
    @JSONField(serialize=false)  
    private List<PackProduct> packProducts;
	
	public List<PackProduct> getPackProducts(){
		if(packProducts == null){
			packProducts = new PackProduct().dao().find("select * from cms_pack_product where packId=?",getId());
        }
        return packProducts;
	}

	
	public List<Pack> findByOrderId(Long orderId){
		return find("select * from cms_pack where orderId=?",orderId);
	}
}
