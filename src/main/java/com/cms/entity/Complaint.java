package com.cms.entity;

import com.cms.entity.base.BaseComplaint;
import com.cms.util.DBUtils;
import com.jfinal.plugin.activerecord.Page;
import org.apache.commons.lang.StringUtils;

/**
 * 投诉功能
 */
@SuppressWarnings("serial")
public class Complaint extends BaseComplaint<Complaint> {

    /**
     * 查找投诉分页
     *
     * @param pageNumber
     *            页码
     * @param pageSize
     *            每页记录数
     * @return 投诉分页
     */
    public Page<Complaint> findPage(String name, Integer pageNumber, Integer pageSize){
        String filterSql = "";
        if(StringUtils.isNotBlank(name)){
            filterSql+= " and name like '%"+name+"%'";
        }
        String orderBySql = DBUtils.getOrderBySql("createDate desc");
        return paginate(pageNumber, pageSize, "select *", "from cms_complaint where 1=1 "+filterSql+orderBySql);
    }

}
