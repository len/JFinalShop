package com.cms.template.method;

import org.apache.commons.lang.StringUtils;

import com.cms.entity.ProductFav;
import com.cms.util.SystemUtils;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;

/**
 * 模板方法 - 公共方法
 * 
 * 
 * 
 */
public class CommonMethod {

	
    public static String abbreviate(String str,Integer width,String ellipsis){
        if (StrKit.isBlank(str) || width == null) {
            return str;
        }
        if(str.length()<=width){
            return str;
        }
        return StringUtils.substring(str, 0, width)+ellipsis;
    }
    
    
    public static String imageUrl(String image){
    	return SystemUtils.getConfig().getImgUrl()+image;
    }
    

    public static String productListUrl(Long productCategoryId,Long brandId,String keyword,Integer pageNumber,String orderBy){
    	String url = JFinal.me().getContextPath()+"/product/list";
    	String params="";
    	if(productCategoryId!=null){
    		params+="&productCategoryId="+productCategoryId;
    	}
    	if(brandId!=null){
    		params+="&brandId="+brandId;
    	}
    	if(StringUtils.isNotBlank(keyword)){
    		params+="&keyword="+keyword;
    	}
    	if(pageNumber!=null){
    		params+="&pageNumber="+pageNumber;
    	}
    	if(StringUtils.isNotBlank(orderBy)){
    		params+="&orderBy="+orderBy;
    	}
    	if(StringUtils.isNotBlank(params)){
    		params ="?"+ params.substring(1);
    	}
    	return url+params;
    }
    
    public static Boolean checkFav(Long memberId,Long productId){
    	ProductFav productFav = new ProductFav().dao().findByMemberId(memberId, productId);
		if(productFav==null){
			return false;
		}
    	return true;
    	
    }
}
