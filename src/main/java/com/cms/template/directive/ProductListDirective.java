package com.cms.template.directive;

import java.util.List;

import com.cms.TemplateVariable;
import com.cms.entity.Product;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

/**
 * 模板指令 - 商品列表
 * 
 * 
 * 
 */
@TemplateVariable(name="product_list")
public class ProductListDirective extends BaseDirective {

	/** "商品分类ID"参数名称 */
	private static final String PRODUCT_CATEGORY_ID_PARAMETER_NAME = "productCategoryId";
	
	/** "标签"参数名称 */
	private static final String TAG_PARAMETER_NAME = "tag";

	/** 变量名称 */
	private static final String VARIABLE_NAME = "products";

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        // TODO Auto-generated method stub
        scope = new Scope(scope);
        Long productCategoryId = getParameter(PRODUCT_CATEGORY_ID_PARAMETER_NAME, Long.class, scope);
        String tag = getParameter(TAG_PARAMETER_NAME, String.class, scope);
        Integer start = getStart(scope);
        Integer count = getCount(scope);
        String condition = getCondition(scope);
        String orderBy = getOrderBy(scope);
        List<Product> products = new Product().dao().findList(productCategoryId,tag,true,start,count,condition,orderBy);
        scope.setLocal(VARIABLE_NAME,products);
        stat.exec(env, scope, writer);
    }

    public boolean hasEnd() {
        return true;
    }
}